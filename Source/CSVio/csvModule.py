'''
Created on 12 Eyl 2015

@author: Gunisigi
'''
import time
import serial
if __name__ == '__main__':
    import csv
    
    ser = serial.Serial(6,115200)  # open first serial port
    print ser.name          # check which port was really used
    
    time.sleep(2)
    
with open('BosphorOffset.csv', 'rb') as f:
    reader = csv.reader(f)
    for row in reader:
        print "x : " + row[0]
        print "y : " + row[1] 
        print "z : " + row[2]
        ser.write([165])
        ser.write([int(row[0])&0xff])
        ser.write([(int(row[0])>>8)&0xff])
        ser.write([int(row[1])&0xff])
        ser.write([(int(row[1])>>8)&0xff])
        ser.write([int(row[2])&0xff])
        ser.write([(int(row[2])>>8)&0xff])
        ser.write([182])
        time.sleep(0.045)
        
    ser.close()   

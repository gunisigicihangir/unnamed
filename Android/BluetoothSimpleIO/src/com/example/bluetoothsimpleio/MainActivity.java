package com.example.bluetoothsimpleio;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.util.Calendar;
import java.util.UUID;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.bluetooth.*;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.example.bluetoothsimpleio.FileIO;

public class MainActivity extends Activity {
	private static final String TAG = "BluetoothSimpleIO";
	
	//!!!!!!!!!!!Bluetooth modulun MAC adresi buraya girilmeli!!!!!!!!!!!!!
	private static String address = "98:D3:31:B0:7C:52";
	//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	//MAC adresi Android baglantisi sirasinda LogCat ekranindan ogrenilebilir.
	//Yukarida yazili bicimde 6 haneli olmalidir.
	
	
	private static final UUID MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	Button btnClick;
	ToggleButton btnToggle;
	TextView txtReceive;
	private BluetoothAdapter btAdapter = null;
	private BluetoothSocket btSocket = null;
	private OutputStream outStream = null;
	private InputStream inpStream = null;
	
	volatile boolean stopWorker;
	Thread workerThread;
	byte[] readBuffer;
	int readBufferPosition;
	int counter;
	
	FileIO m_fileio;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		m_fileio = new FileIO();
		
		btnClick = (Button) findViewById(R.id.button1);
		btnToggle = (ToggleButton) findViewById(R.id.toggleButton1);
		txtReceive = (TextView) findViewById(R.id.textView2);
		
		// Android Bluetooth baglantisini olustur
		btAdapter = BluetoothAdapter.getDefaultAdapter();
		if(btAdapter==null) { 
			Log.d(TAG, "...Bluetooth desteklenmiyor...");
	      finish();
	    } else {
	      if (btAdapter.isEnabled()) {
	    	//Android baglantisi acildi
	        Log.d(TAG, "...Bluetooth ON...");
	      } else {
	        //Bluetooth cihazi kapali. Kullaniciya acmasini soyle.
	        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
	        startActivityForResult(enableBtIntent, 1);
	      }
	    }
		//Butona basildiginda Bluetooth komutu gonder
		btnClick.setOnTouchListener(new View.OnTouchListener() {
			 @Override
			 public boolean onTouch(View arg0, MotionEvent arg1) {
		            if (arg1.getAction()==MotionEvent.ACTION_DOWN)
		            {
		            	// Butona basilinca "O" komutunu gonder
		            	Log.d(TAG, "...Bluetooth veri gonder: O" );
		            	//sendData("O");
		            	startCommunication();
		            }
		            else if (arg1.getAction()==MotionEvent.ACTION_UP)
		            {
		            	// Butona basilinca "C" komutunu gonder
		            	Log.d(TAG, "...Bluetooth veri gonder: C" );
		            	//sendData("C");
		            	stopCommunication();
		            }
		            return true;
		        }

		    });
		
		//Toggle butonuna basinca Bluetooth komutu gonder
		btnToggle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (btnToggle.isChecked())
				{
					// Toggle aktif ise "O" komutunu gonder
					//sendData("O");
					startCommunication();
					Log.d(TAG, "...Bluetooth veri gonder: O" );
				}
				 else 
				 {
					 // Toggle aktif degilse "C" komutunu gonder
					//sendData("C");
					 stopCommunication();
					Log.d(TAG, "...Bluetooth veri gonder: C" );
				 }
				
			}
		});
		
	}
	
	public void startCommunication()
	{
		Calendar c = Calendar.getInstance(); 
		//int seconds = c.get(Calendar.DAY_OF_MONTH);
		String fname = String.valueOf(c.get(Calendar.MONTH)) + String.valueOf( c.get(Calendar.DAY_OF_MONTH)) + String.valueOf(c.get(Calendar.HOUR_OF_DAY)) + String.valueOf(c.get(Calendar.MINUTE)) + String.valueOf(c.get(Calendar.SECOND)) + ".txt";
		m_fileio.openFileForWrite(fname);
		sendData("O");
	}
	
	public void stopCommunication()
	{
		sendData("C");
		m_fileio.closeFile();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	// Bluetooth soketini olustur
	private BluetoothSocket createBluetoothSocket(BluetoothDevice device) throws IOException {
	      if(Build.VERSION.SDK_INT >= 10){
	          try {
	              final Method  m = device.getClass().getMethod("createInsecureRfcommSocketToServiceRecord", new Class[] { UUID.class });
	              return (BluetoothSocket) m.invoke(device, MY_UUID);
	          } catch (Exception e) {
	              Log.e(TAG, "RFComm baglantisi saglanamadi!",e);
	          }
	      }
	      return  device.createRfcommSocketToServiceRecord(MY_UUID);
	  }
	
	@Override
	  public void onResume() {
	    super.onResume();
	 
	    Log.d(TAG, "...onResume - baglaniyor...");
	   
	    // Bluetooth cihazi icin pointer olustur.
	    BluetoothDevice device = btAdapter.getRemoteDevice(address);
	   
		try {
			// Soket yukarida olusturulan pointer kullanilarak olusturuluyor.
			btSocket = createBluetoothSocket(device);
		} catch (IOException e1) {
			// Soket olusturulamadi
			errorExit("Fatal Error", "onResume() soket olusturma basarisiz: " + e1.getMessage() + ".");
		}
	   
	    // Cihaz tarama cihaz baglantisi sirasinda kapatiliyor.
	    btAdapter.cancelDiscovery();
	   
	    Log.d(TAG, "...Baglaniyor...");
	    try {
	      btSocket.connect();
	      Log.d(TAG, "...Baglandi...");
	    } catch (IOException e) {
	      try {
	        btSocket.close();
	      } catch (IOException e2) {
	        errorExit("Fatal Error", "onResume() Soket kapatilamadi" + e2.getMessage() + ".");
	      }
	    }
	     
	    // Sunucuyla haberlesmek icin bir veri dizisi olustur
	    Log.d(TAG, "...Soket olustur...");
	 
	    try {
	      outStream = btSocket.getOutputStream();
	    } catch (IOException e) {
	      errorExit("Fatal Error", "onResume() Cikis dizisi olusturlamadi:" + e.getMessage() + ".");
	    }
	    
	    try {
		      inpStream = btSocket.getInputStream();
		    } catch (IOException e) {
		      errorExit("Fatal Error", "onResume() Giris dizisi olusturlamadi:" + e.getMessage() + ".");
		    }
	    
	    beginListenForData();
	    
	  }
	 
	  @Override
	  public void onPause() {
	    super.onPause();
	 
	    Log.d(TAG, "...onPause()...");
	 
	    if (outStream != null) {
	      try {
	        outStream.flush();
	      } catch (IOException e) {
	        errorExit("Fatal Error", "onPause() cikis dizisi bosaltilamadi: " + e.getMessage() + ".");
	      }
	    }
	 
	    try     {
	      btSocket.close();
	    } catch (IOException e2) {
	      errorExit("Fatal Error", "onPause() soket kapatilamadi." + e2.getMessage() + ".");
	      
	    }
	    
	    stopWorker = true;
	    
	  }
	
	// Mesaj outStream kullanilarak gonderiliyor
	private void sendData(String message) {
	    byte[] msgBuffer = message.getBytes();
	 
	    Log.d(TAG, "...Bluetooth mesaj: " + message + "...");
	 
	    try {
	      outStream.write(msgBuffer);
	    } catch (IOException e) {
	      	Log.d(TAG, "...Bluetooth Veri Hatasi... e:" + e.getMessage());      
	      	errorExit("Fatal Error", "Bluetooth: " + e.getMessage() + ".");
	    }
	  }
	
	
	void beginListenForData()
	{
	    final Handler handler = new Handler(); 
	    final byte delimiter = 10; //This is the ASCII code for a newline character

	    stopWorker = false;
	    readBufferPosition = 0;
	    readBuffer = new byte[1024];
	    workerThread = new Thread(new Runnable()
	    {
	        public void run()
	        {                
	           while(!Thread.currentThread().isInterrupted() && !stopWorker)
	           {
	                try 
	                {
	                    int bytesAvailable = inpStream.available();  
	                    if (bytesAvailable > 0)
	                    {
	                    	byte[] start = new byte[1];
	                    	inpStream.read(start,0,1);
	                    	if (start[0] == -91)
	                    	{
	                    		while(inpStream.available()<7); 
	                    		byte[] packetBytes = new byte[7]; //bytesAvailable];
    	                        inpStream.read(packetBytes,0,7);
    	                        
    	                        if (packetBytes[6]==-74)
	    	                    {
	    	                        	//byte b = packetBytes[i];
	    	                        	//if ((packetBytes[0] == 165) && (packetBytes[7] == 182))
	    	                        	{
	    	                        		short axisX = (short) (((packetBytes[1]&0xff)<<8) | (packetBytes[0]&0xff));
	    	                        		short axisY = (short) (((packetBytes[3]&0xff)<<8) | (packetBytes[2]&0xff));
	    	                        		short axisZ = (short) (((packetBytes[5]&0xff)<<8) | (packetBytes[4]&0xff));
	    	                        	    final String axes = Short.toString(axisX) + "," + Short.toString(axisY) + "," + Short.toString(axisZ);
	    	                        	    m_fileio.writeBytes(axes.getBytes());
	    	                        		//readBufferPosition = 0;

	    	                        		handler.post(new Runnable()
	    	                        		{
	    	                        			public void run()
	    	                        			{
	    	                        				txtReceive.setText(axes);
	    	                        			}
	    	                        		});
	    	                        }
	    	                    }
	                    	}
	                    }
	                    
	                } 
	                catch (IOException ex) 
	                {
	                    stopWorker = true;
	                }
	           }
	        }
	    });

	    workerThread.start();
	}
	
	
	private void errorExit(String title, String message){
	    Toast.makeText(getBaseContext(), title + " - " + message, Toast.LENGTH_LONG).show();
	    finish();
	  }

}

package com.example.bluetoothsimpleio;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Environment;

public class FileIO {

	FileOutputStream f;
	
	public void openFileForWrite(String FileName)
	{
		File sdCard = Environment.getExternalStorageDirectory();
		File dir = new File (sdCard.getAbsolutePath() + "/riverData");
		dir.mkdirs();
		File file = new File(dir, FileName);

		try {
			f = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void closeFile()
	{
		try {
			f.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void writeBytes(byte[] input)
	{
		try {
			byte[] c = new byte[input.length + 1];
			byte[] lf = new byte[1];
			lf[0]=10;
			System.arraycopy(input, 0, c, 0, input.length);
			System.arraycopy(lf, 0, c, input.length, lf.length);
			f.write(c, 0, c.length);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}

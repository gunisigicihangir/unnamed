#include <TimerOne.h>

/*
Arduino baglantisi :
Bluetooth Rx : Arduino pin 1(Tx)
Bluetooth Tx : Arduino pin 0(Rx)
out1 : Arduino pin 5
*/
#include <Wire.h>

//Accelerometer Settings:////////////////////////////////////
const int  ADXL345 = 0x53;        //Accelerometer Address
//Registers
const char POWER_CTL = 0x2D;    //Power Control Register
const char DATA_FORMAT = 0x31;  //Data Format Register
const char DATAX0 = 0x32;    //X-Axis Data 0
const char INT_ENABLE = 0x2E;   //Interrupt Enable Register
const char THRESH_ACT = 0x24;   //Interrupt Threshold Register
const char ACT_INACT_CTL = 0x27;//Activity Inactivity Set Register
const char BW_RATE = 0x2C;      //Bandwith and Rate Register
const char INT_MAP = 0x2F;      //Interrupt Map Register
const char INT_SOURCE = 0x30;   //Interrupt Source Register
const char THRESH_FF = 0x28;    //FreeFall Threshold
const char TIME_FF = 0x29;      //FreeFall time threshold  

char incomingByte;  // gelen veri
int  out1 = 5;   //Cikis
int  ledb = 13;  // LED pin


char* buffer;

boolean started = false;

long time = 0;
long tmp_time = 0;
 
void setup() {
  Wire.begin();        // join i2c bus (address optional for master)
  // Bluetooth modullerde seri port hizi degisiklik
  // gosterebilir. 9600 ile veri alinmiyorsa 38400
  // denenebilir.
  //Serial.begin(9600); // Seri portu baslat
  pinMode(1, OUTPUT);
  pinMode(0, INPUT);
  Serial1.begin(9600); // Seri portu baslat Leonardo
   while (!Serial1) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  pinMode(out1, OUTPUT);
  pinMode(ledb, OUTPUT);
  digitalWrite(ledb, LOW);
  
  Serial.begin(9600);
   //while (!Serial) {
   // ; // wait for serial port to connect. Needed for Leonardo only
  //}
  
  started = false;
  
  // Prepare device for 4g data format
      Wire.beginTransmission(ADXL345);
      Wire.write(DATA_FORMAT);
      Wire.write(2); //4g data format
      Wire.endTransmission();
    
    
    //  // Measure activity
      Wire.beginTransmission(ADXL345);
      Wire.write(POWER_CTL);
      Wire.write(8); //Measure Activity
      Wire.endTransmission();
  
  delay(1000);
}
 
void loop() {
  
  /*time = millis();
  Serial.println((time-tmp_time),DEC);
  tmp_time = time;*/
  
  if (Serial1.available() > 0) {
    incomingByte = Serial1.read();
    if(incomingByte == 'C') { // Gelen karakter "C" (ASCII:67)
       Serial.write("C");
       Serial1.write(incomingByte);
       Serial1.write(10);
       started = false;
       //digitalWrite(ledb, LOW);
    } else if(incomingByte == 'O') { // Gelen karakter "O" (ASCII:79)
       Serial.write("O");
       Serial1.write(incomingByte);
       Serial1.write(10);
       started = true;
       //digitalWrite(ledb, HIGH);
    }
    
  }
  
  sample();
  
  delay(49);
}

void sample()
{
  if (started)
  {
      int axisX=0;
      int axisY=0;
      int axisZ=0;
      char accBuff[6];
      
      int i=0;
      
      // Request data from the device
      Wire.beginTransmission(ADXL345);
      // Request 6 bytes starting from X-Axis Data 0
      Wire.write(DATAX0);
      Wire.endTransmission();
      
      // Read 6 bytes
      Wire.requestFrom(ADXL345, 6);
      while(Wire.available())
      { 
        accBuff[i++] = Wire.read();
      }
      Wire.endTransmission();
      
      /*axisX = int(accBuff[1]<<8) | accBuff[0];
      axisY = int(accBuff[3]<<8) | accBuff[2];
      axisZ = int(accBuff[5]<<8) | accBuff[4];*/
      
      Serial1.write(0xA5);
      Serial1.write(accBuff[0]);
      Serial1.write(accBuff[1]);
      Serial1.write(accBuff[2]);
      Serial1.write(accBuff[3]);
      Serial1.write(accBuff[4]);
      Serial1.write(accBuff[5]);
      Serial1.write(0xB6);
      
      /*Serial.print(axisX);
      Serial.print(axisY);
      Serial.println(axisZ);*/
      
      digitalWrite(out1, HIGH);
      digitalWrite(ledb, HIGH);
  } else {
      digitalWrite(out1, LOW); 
      digitalWrite(ledb, LOW);
  }
}

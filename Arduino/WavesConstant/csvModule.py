'''
Created on 12 Eyl 2015

@author: Gunisigi
'''
import time
import serial
if __name__ == '__main__':
    import csv
    
    # ser = serial.Serial(6,115200)  # open first serial port
    # print ser.name          # check which port was really used
    
    # time.sleep(2)
    
with open('BosphorOffset.csv', 'rb') as f:
    reader = csv.reader(f)

    lineCount = 0;
    data = "";
    
    for row in reader:
        a=int(row[0]);
        if (a<0):
            valx=256+a;
        else:
            valx=a;

        a=int(row[1]);
        if (a<0):
            valy=256+a;
        else:
            valy=a;
            
        data += (format(valx, '#04x') + ", " + format(valy, '#04x') + ", ");

        lineCount += 1;

    print("const byte constantData[" + str(lineCount * 2) + "] PROGMEM = { " + data + " };");
        
    # ser.close()   

/*
Arduino baglantisi :
Bluetooth Rx : Arduino pin 1(Tx)
Bluetooth Tx : Arduino pin 0(Rx)
out1 : Arduino pin 5
*/

char incomingByte;  // gelen veri
int  out1 = 5;   //Cikis
int  ledb = 13;  // LED pin


char* buffer;

 
void setup() {
  // Bluetooth modullerde seri port hizi degisiklik
  // gosterebilir. 9600 ile veri alinmiyorsa 38400
  // denenebilir.
  //Serial.begin(9600); // Seri portu baslat
  pinMode(1, OUTPUT);
  pinMode(0, INPUT);
  Serial1.begin(9600); // Seri portu baslat Leonardo
   while (!Serial1) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  pinMode(out1, OUTPUT);
  pinMode(ledb, OUTPUT);
  digitalWrite(ledb, LOW);
  
  Serial.begin(9600);
   //while (!Serial) {
   // ; // wait for serial port to connect. Needed for Leonardo only
  //}
}
 
void loop() {
  if (Serial1.available() > 0) {  // Gelen veri var mi?
    incomingByte = Serial1.read(); // Veriyi oku
    //digitalWrite(ledb, HIGH);
    Serial.write(incomingByte);
    if(incomingByte == 'C') { // Gelen karakter "C" (ASCII:67)
       digitalWrite(out1, LOW); 
       digitalWrite(ledb, LOW);
       Serial1.write(incomingByte);
       Serial1.write(10);
    } else if(incomingByte == 'O') { // Gelen karakter "O" (ASCII:79)
       digitalWrite(out1, HIGH);
       digitalWrite(ledb, HIGH);
       Serial1.write(incomingByte);
       Serial1.write(10);
    }
    
  }
  
}

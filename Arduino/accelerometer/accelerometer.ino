#include <Wire.h>

//Accelerometer Settings:////////////////////////////////////
const int  ADXL345 = 0x53;        //Accelerometer Address
//Registers
const char POWER_CTL = 0x2D;    //Power Control Register
const char DATA_FORMAT = 0x31;  //Data Format Register
const char DATAX0 = 0x32;    //X-Axis Data 0
const char INT_ENABLE = 0x2E;   //Interrupt Enable Register
const char THRESH_ACT = 0x24;   //Interrupt Threshold Register
const char ACT_INACT_CTL = 0x27;//Activity Inactivity Set Register
const char BW_RATE = 0x2C;      //Bandwith and Rate Register
const char INT_MAP = 0x2F;      //Interrupt Map Register
const char INT_SOURCE = 0x30;   //Interrupt Source Register
const char THRESH_FF = 0x28;    //FreeFall Threshold
const char TIME_FF = 0x29;      //FreeFall time threshold  

void setup(){
  Wire.begin();        // join i2c bus (address optional for master)
  Serial.begin(9600);  // start serial for output
}
void loop() {
  int axisX=0;
  int axisY=0;
  int axisZ=0;
  byte accBuff[6];
  
// Prepare device for 4g data format
  Wire.beginTransmission(ADXL345);
  Wire.write(DATA_FORMAT);
  Wire.write(2); //4g data format
  Wire.endTransmission();


//  // Measure activity
  Wire.beginTransmission(ADXL345);
  Wire.write(POWER_CTL);
  Wire.write(8); //Measure Activity
  Wire.endTransmission();


while(1){
  int i=0;
  
  // Request data from the device
  Wire.beginTransmission(ADXL345);
  // Request 6 bytes starting from X-Axis Data 0
  Wire.write(DATAX0);
  Wire.endTransmission();
  
  // Read 6 bytes
  Wire.requestFrom(ADXL345, 6);
  while(Wire.available())
  { 
    accBuff[i++] = Wire.read();
  }
  Wire.endTransmission();
  
  axisX = int(accBuff[1]<<8) | accBuff[0];
  axisY = int(accBuff[3]<<8) | accBuff[2];
  axisZ = int(accBuff[5]<<8) | accBuff[4];
  
  //Serial.println(axisZ);
  //Serial.print("Axis X: ");
  //Serial.println(axisX);
  Serial.print("Axis Y: ");
  Serial.println(axisY);
  
  
  delay(100);
}
}

/*void getAccelerometerData(){
  int i=0;
  
  // Request data from the device
  Wire.beginTransmission(ADXL345);
  // Request 6 bytes starting from X-Axis Data 0
  Wire.write(DATAX0);
  Wire.endTransmission();
  
  // Read 6 bytes
  Wire.requestFrom(ADXL345, 6);
  while(Wire.available())
  { 
    accBuff[i++] = Wire.read();
  }
  Wire.endTransmission();
}*/

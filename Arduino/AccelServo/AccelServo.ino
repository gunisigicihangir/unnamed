#include <Servo.h> 
#include <Wire.h>

#define USE_SERIAL_DATA 1
// #define USE_ACCEL_DATA 1



const int servoMax = 65;
const int servoMin = 20;
const int servoSteady = 35;

const int accelMax = 40;

const int xOffset = 0;
const int yOffset = 0;

const int xLevel = 0;
const int yLevel = 0;

const int servoAStd = 40;
const int servoCStd = 40;

const int servoBStd = 40;
const int servoDStd = 43;

const int maxServoChange = 20;

const int moveTolerance = 3;

//Accelerometer Settings:////////////////////////////////////
const int  ADXL345 = 0x53;        //Accelerometer Address
//Registers
const char POWER_CTL = 0x2D;    //Power Control Register
const char DATA_FORMAT = 0x31;  //Data Format Register
const char DATAX0 = 0x32;    //X-Axis Data 0
const char INT_ENABLE = 0x2E;   //Interrupt Enable Register
const char THRESH_ACT = 0x24;   //Interrupt Threshold Register
const char ACT_INACT_CTL = 0x27;//Activity Inactivity Set Register
const char BW_RATE = 0x2C;      //Bandwith and Rate Register
const char INT_MAP = 0x2F;      //Interrupt Map Register
const char INT_SOURCE = 0x30;   //Interrupt Source Register
const char THRESH_FF = 0x28;    //FreeFall Threshold
const char TIME_FF = 0x29;      //FreeFall time threshold  

Servo srArmA;
Servo srArmB;
Servo srArmC;
Servo srArmD;

// Servo channels
int armA = 3;
int armB = 6;
int armC = 9;
int armD = 5;

int tmpX = 0;
int tmpY = 0;

void setup() 
{ 
  #if defined(USE_SERIAL_DATA)
  initSerialPort();
  #endif
  
  #if defined(USE_ACCEL_DATA)
  initAccelerometer();
  #endif
  
  initServos();
}


void loop() 
{ 
  byte nullArray[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };
  #if defined(USE_SERIAL_DATA)
   byte* accelBuffer = readPositionFromSerialPort(); 
  #else
  byte* accelBuffer = readPositionFromAccelerometer();
  #endif
  // Every axis has two-byte-data for position
  int axisX = int(accelBuffer[1]<<8) | accelBuffer[0];
  int axisY = int(accelBuffer[3]<<8) | accelBuffer[2];
  int axisZ = int(accelBuffer[5]<<8) | accelBuffer[4];
  
  moveServos( transformPosition(axisX+xOffset), transformPosition(axisY+yOffset));
        
  //delay(10);
  
}
void initSerialPort()
{
Serial.begin(115200); 
Serial.setTimeout(100000);
}
void initAccelerometer()
{
  Wire.begin();
  
// Prepare device for 4g data format
  Wire.beginTransmission(ADXL345);
  Wire.write(DATA_FORMAT);
  Wire.write(2); //4g data format
  Wire.endTransmission();


//  // Measure activity
  Wire.beginTransmission(ADXL345);
  Wire.write(POWER_CTL);
  Wire.write(8); //Measure Activity
  Wire.endTransmission();  
}

#if defined(USE_SERIAL_DATA)
byte* readPositionFromSerialPort()
{
  byte serbuffer[8];
  byte posBuffer[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};  
  //Serial.readBytes(serbuffer, 8);
  
  while (Serial.available() <8);
  
  if (Serial.available() > 7)
  {
    for(int i=0;i<8;i++)
      serbuffer[i] = Serial.read();
    //Serial.readBytes(serbuffer, 8);
  }
  
  if ( ( int(serbuffer[0] & 0xff) == 165 ) && ( int(serbuffer[7] & 0xff) == 182 ) )
  {
    for (int i=0; i<6; i++)
      posBuffer[i] = serbuffer[i+1];
      
    /*for (int i=0;i<6;i++)
      Serial.write(posBuffer[i]);*/
      delay(0);
  }

  /*for (int i=0;i<6;i++)
      Serial.write(posBuffer[i]);*/

  return posBuffer;
  
}
#endif


#if defined(USE_ACCEL_DATA)
byte* readPositionFromAccelerometer()
{
  int i=0;
  byte accBuff[6];
  // Request data from the device
  Wire.beginTransmission(ADXL345);
  // Request 6 bytes starting from X-Axis Data 0
  Wire.write(DATAX0);
  Wire.endTransmission();
  
  // Read 6 bytes
  Wire.requestFrom(ADXL345, 6);
  while(Wire.available())
  { 
    accBuff[i++] = Wire.read();
  }
  Wire.endTransmission();
  
  return accBuff;
}
#endif

void initServos()
{
  srArmA.attach(armA,500,1800);
  srArmB.attach(armB,500,1800);
  srArmC.attach(armC,500,1800);
  srArmD.attach(armD,500,1800);
  
  srArmA.write(servoAStd);
  srArmB.write(servoBStd);
  srArmC.write(servoCStd);
  srArmD.write(servoDStd);
}

void moveServos(int xPosition, int yPosition)
{
  int servoPos = 0;
  //if ( abs(abs(xPosition) - abs(tmpX)) > moveTolerance )
  {
    servoPos = (servoAStd - maxServoChange) + (xPosition);
    srArmA.write( servoPos );
    servoPos = (servoCStd + maxServoChange) - (xPosition);
    srArmC.write( servoPos );
    tmpX = xPosition;
  }
  
  //if ( abs(abs(yPosition) - abs(tmpY)) > moveTolerance )
  {  
    servoPos = (servoBStd - maxServoChange) + (yPosition);
    srArmB.write( servoPos );
    servoPos = (servoDStd + maxServoChange) - (yPosition);
    srArmD.write( servoPos );
    tmpY = yPosition;
  }
  
}

int transformPosition(int trPosition)
{
  if ( abs(trPosition) > accelMax)
    trPosition = accelMax * ( trPosition / abs(trPosition)); // Don't lose sign
    
  int normalizedPosition = trPosition + accelMax;
  
  int scaledPosition = (int)( ( normalizedPosition * ( 2 * maxServoChange ) ) / ( 2 * accelMax ) );
  
  return scaledPosition;
}


#include <Servo.h> 
 
Servo myservo;
Servo myservo2;
Servo myservo3;
Servo myservo4;

int armA = 3;
int armB = 5;
int armC = 6;
int armD = 9;

 
int pos = 0;    // variable to store the servo position 
 
void setup() 
{ 
  Serial.begin(9600);
  myservo.attach(armA,750,1800);  // attaches the servo on pin 9 to the servo object 
  //delay(1000);
  myservo2.attach(armB,750,1800);
  //delay(1000);
  myservo3.attach(armC,750,1800);
  //delay(1000);
  myservo4.attach(armD,750,1800);
  delay(1000);
  
  int posCons = 70;
  
  myservo.write(posCons);              // tell servo to go to position in variable 'pos' 
  myservo2.write(posCons);
  myservo3.write(posCons);
  myservo4.write(posCons);

/*  pinMode(armA,OUTPUT);
  pinMode(armB,OUTPUT);
  pinMode(armC,OUTPUT);
  pinMode(armD,OUTPUT);
  setPwmFrequency(armA,128);
  setPwmFrequency(armB,512);
  setPwmFrequency(armC,512);
  setPwmFrequency(armD,128);*/

  Serial.println("Setup done");
  
  //delay(1000);
  
  
  
} 
 
 
void loop() 
{ 
  /*char incomingByte[3];
  if (Serial.available()>2)
  {
    Serial.readBytes(incomingByte,3);
    pos = incomingByte[0]<<16 | incomingByte[1]<<8 | incomingByte[2];
    Serial.print(pos,DEC);
  }*/
  //int pos = 60;
  int minPos = 40;
  int maxPos = 100;

  for(pos = minPos; pos < maxPos; pos += 1) 
  {                                  
    myservo.write(minPos+(maxPos-pos)); 
    //myservo2.write(pos);
    myservo3.write(pos);
    //myservo4.write(pos);    
    delay(10);                       
  }

  for(pos = maxPos; pos>=minPos; pos-=1)
  {                                
    myservo.write(minPos+(maxPos-pos));
    //myservo2.write(50+(70-pos));
    myservo3.write(pos);
    //myservo4.write(pos);
    
    delay(10);
  } 

/*analogWrite(armA,pos);
    analogWrite(armB,pos);
    analogWrite(armC,pos);
    analogWrite(armD,pos);*/


} 



/**
 * Divides a given PWM pin frequency by a divisor.
 *
 * The resulting frequency is equal to the base frequency divided by
 * the given divisor:
 *   - Base frequencies:
 *      o The base frequency for pins 3, 9, 10, and 11 is 31250 Hz.
 *      o The base frequency for pins 5 and 6 is 62500 Hz.
 *   - Divisors:
 *      o The divisors available on pins 5, 6, 9 and 10 are: 1, 8, 64,
 *        256, and 1024.
 *      o The divisors available on pins 3 and 11 are: 1, 8, 32, 64,
 *        128, 256, and 1024.
 *
 * PWM frequencies are tied together in pairs of pins. If one in a
 * pair is changed, the other is also changed to match:
 *   - Pins 5 and 6 are paired on timer0
 *   - Pins 9 and 10 are paired on timer1
 *   - Pins 3 and 11 are paired on timer2
 *
 * Note that this function will have side effects on anything else
 * that uses timers:
 *   - Changes on pins 3, 5, 6, or 11 may cause the delay() and
 *     millis() functions to stop working. Other timing-related
 *     functions may also be affected.
 *   - Changes on pins 9 or 10 will cause the Servo library to function
 *     incorrectly.
 *
 * Thanks to macegr of the Arduino forums for his documentation of the
 * PWM frequency divisors. His post can be viewed at:
 *   http://www.arduino.cc/cgi-bin/yabb2/YaBB.pl?num=1235060559/0#4
 */
void setPwmFrequency(int pin, int divisor) {
  byte mode;
  if(pin == 5 || pin == 6 || pin == 9 || pin == 10) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 64: mode = 0x03; break;
      case 256: mode = 0x04; break;
      case 1024: mode = 0x05; break;
      default: return;
    }
    if(pin == 5 || pin == 6) {
      TCCR0B = TCCR0B & 0b11111000 | mode;
    } else {
      TCCR1B = TCCR1B & 0b11111000 | mode;
    }
  } else if(pin == 3 || pin == 11) {
    switch(divisor) {
      case 1: mode = 0x01; break;
      case 8: mode = 0x02; break;
      case 32: mode = 0x03; break;
      case 64: mode = 0x04; break;
      case 128: mode = 0x05; break;
      case 256: mode = 0x06; break;
      case 1024: mode = 0x7; break;
      default: return;
    }
    TCCR2B = TCCR2B & 0b11111000 | mode;
  }
}
